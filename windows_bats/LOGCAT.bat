@echo off
echo ------------------ LOGGING ------------------
set name=log_%date%_%time:~0,2%_%time:~3,2%_%time:~6,2%.txt
echo %name%
adb logcat -s Unity -d > %name%
echo ------------------ FINISHED ------------------